namespace :db do
  desc "Fill in database with sample data"
  task :populate => :environment do
    make_users
    make_microposts
    users = User.all
    user = users.first
    following = users[0..50]
    followers = users[3..40]
    following.each {|followed| user.follow!(followed)}
    followers.each {|follower| follower.follow!(user)}
  end
end

def make_users
  Rake::Task['db:reset'].invoke
  admin = User.create!(:name => "Example User",
                       :email => "example@example.org",
                       :password => "foobar",
                       :password_confirmation => "foobar")
  admin.toggle!(:admin)
  99.times do |n|
    name = Faker::Name.name
    email = "example-#{n+1}@example.org"
    password = "password"
    User.create!(:name => name,
                 :email => email,
                 :password => password,
                 :password_confirmation => password)
  end
end


def make_microposts
  50.times do
    User.all(:limit => 6).each do |user|
      user.microposts.create!(:content => Faker::Lorem.sentence(5))
    end
  end
end

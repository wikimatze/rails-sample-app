module ApplicationHelper

  def title
    base_title = "Matthias Guenther"
    if @title.nil?
      base_title
    else
      "#{base_title} | #{@title}"
    end
  end

  def logo
    image_tag("rails.png", :alt => "Sample App", :class => "round")
  end

  def profile_link
    if signed_in?
      link = link_to "Profile", current_user
      content_tag(:li, link)
    end
  end

  def settings_path
    if signed_in?
      link = link_to "Settings", edit_user_path(current_user)
      content_tag(:li, link)
    end
  end

  def users_link
    if signed_in?
      link = link_to "Users", users_path
      content_tag(:li, link)
    end
  end

  def signin_signout_link
    if signed_in?
      link = link_to 'Sign out', signout_path, :method => :delete
      content_tag(:li, link)
    else
      link = link_to 'Sign in', signin_path
      content_tag(:li, link)
    end
  end

end

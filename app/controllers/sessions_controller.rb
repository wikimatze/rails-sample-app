class SessionsController < ApplicationController

  def new
    @title = "Sign in"
  end

  def create
    email = params[:session][:email]
    password = params[:session][:password]
    user = User.authenticate(email, password)

    if user.nil?
      flash.now[:error] = "Invalid email/password combination"
      # create error message and re-render signin form
      @title = "Sign in"
      render 'new'
    else
      # sign in user and redirect to the user's show page
      sign_in user
      redirect_back_or user
    end
  end

  def destroy
    sign_out
    redirect_to(root_path)
  end

end

# == Schema Information
#
# Table name: users
#
#  id                 :integer         not null, primary key
#  name               :string(255)
#  email              :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  encrypted_password :string(255)
#  salt               :string(255)
#

require 'spec_helper'

describe User do

  before(:each) do
    @attr = {
      :name => 'Matthias',
      :email => 'matthias.guenther@wikimatze.de',
      :password => 'foobar',
      :password_confirmation => 'foobar'
    }
  end

  it "should create a new instance with the given attributes" do
    User.create(@attr)
  end

  it "should require a name" do
    no_name_user = User.new(@attr.merge(:name => ''))
    no_name_user.should_not be_valid
  end

  it "should require an email" do
    no_email_user = User.new(@attr.merge(:email => ''))
    no_email_user.should_not be_valid
  end

  it "should reject a too long name" do
    name = "a" * 52
    long_name_user = User.new(@attr.merge(:name => name))
    long_name_user.should_not be_valid
  end

  it "should accept valid email addresses" do
    email_addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    email_addresses.each do |address|
      valid_email_user = User.new(@attr.merge(:email => address))
      valid_email_user.should be_valid
    end
  end

  it "should reject invalid email addresses" do
    email_addresses = %w[user@foo,test THE_USER_at_foo.bar.org]
    email_addresses.each do |address|
      invalid_email_address = User.new(@attr.merge(:email => address))
      invalid_email_address.should_not be_valid
    end
  end

  it "should reject reject duplicate email addresses" do
    User.create!(@attr)
    user_with_duplicated_email = User.new(@attr)
    user_with_duplicated_email.should_not be_valid
  end

  it "should reject email addresses identical up to case" do
    upcased_email = @attr[:email].upcase
    User.create!(@attr.merge(:email => upcased_email))
    user_with_duplicated_email = User.new(@attr)
    user_with_duplicated_email.should_not be_valid
  end

  describe "password validations" do
    it "should require a password" do
      user_empty_password = User.new(@attr.merge(:password => '', :password_confirmation => ''))
      user_empty_password.should_not be_valid
    end

    it "should require a matching password confirmation" do
      user_wrong_confirmation = User.new(@attr.merge(:password_confirmation => 'invalid'))
      user_wrong_confirmation.should_not be_valid
    end

    it "should reject short passwords" do
      phrase = "a" * 5
      user_short_pass = User.new(@attr.merge(:password => phrase, :password_confirmation => phrase))
      user_short_pass.should_not be_valid
    end

    it "should reject long passwords" do
      phrase = "a" * 100
      user_long_pass = User.new(@attr.merge(:password => phrase, :password_confirmation => phrase))
      user_long_pass.should_not be_valid
    end
  end

  describe "password encryption" do

    before(:each) do
      @user = User.create!(@attr)
    end

    it "should have an encrypted password attribute" do
      @user.should respond_to(:encrypted_password)
    end

    it "should set the encrypted password" do
      @user.encrypted_password.should_not be_blank
    end

    describe "has_password? method" do
      it "should be true if the passwords match" do
        @user.has_password?(@attr[:password])
      end

      it "should be false if the passwords don't match" do
        @user.has_password?("invalid").should be_false
      end
    end

    describe "authenticate method" do
      it "should return nil on email/password mismatch" do
        wrong_password_user = User.authenticate(@attr[:email], "wrong")
        wrong_password_user.should be_nil
      end

      it "should return nil for an email address with no user" do
        nonexistent_user = User.authenticate("foo@bar.com", @attr[:password])
        nonexistent_user.should be_nil
      end

      it "should return user on email/password match" do
        user_matching = User.authenticate(@attr[:email], @attr[:password])
        user_matching.should == @user
      end
    end
  end

  describe "admin attribute" do

    before(:each) do
      @user = User.create!(@attr)
    end

    it "should respond to admin" do
      @user.should respond_to(:admin)
    end

    it "should not be an admin by default" do
      @user.should_not be_admin
    end

    it "should be convertable to admin" do
      @user.toggle!(:admin)
      @user.should be_admin
    end
  end

  describe "micropost associations" do

    before(:each) do
      @user = User.create(@attr)
      @micropost_one = Factory(:micropost, :user => @user, :created_at => 1.day.ago)
      @micropost_two = Factory(:micropost, :user => @user, :created_at => 1.hour.ago)
      @micropost_array = []
      @micropost_array << @micropost_two << @micropost_one
    end

    it "should have the microposts attribute" do
      @user.should respond_to(:microposts)
    end

    it "should have the right microposts in the right oder" do
      @user.microposts.should == @micropost_array
    end

    it "should destroy associated microposts" do
      @user.destroy
      @micropost_array.each do |micropost|
        Micropost.find_by_id(micropost.id).should be_nil
      end
    end

    describe "status feed" do

      it "should have a feed" do
        @user.should respond_to(:feed)
      end

      it "should include the user's microposts" do
        @user.feed.include?(@micropost_one).should be_true
        @user.feed.include?(@micropost_two).should be_true
      end

      it "should not include a different user's microposts" do
        micropost_three = Factory(:micropost, :user => Factory(:user, :email => Factory.next(:email)))
        @user.feed.include?(micropost_three).should be_false
      end

      it "should include the microposts of followed users" do
        followed = Factory(:user, :email => Factory.next(:email))
        micropost_three = Factory(:micropost, :user => followed)
        @user.follow!(followed)
        @user.feed.should include(micropost_three)
      end
    end
  end

  describe "relationships" do

    before(:each) do
      @user = User.create!(@attr)
      @followed = Factory(:user)
    end

    it "should have a relationships method" do
      @user.should respond_to(:relationships)
    end

    it "should have the following method" do
      @user.should respond_to(:following)
    end

    it "should have a following? method" do
      @user.should respond_to(:following?)
    end

    it "should have a follow! method" do
      @user.should respond_to(:follow!)
    end

    it "should follow another user" do
      @user.follow!(@followed)
      @user.should be_following(@followed)
    end

    it "should have the unfollow! method" do
      @user.should respond_to(:unfollow!)
    end

    it "should unfollow a user" do
      @user.follow!(@followed)
      @user.unfollow!(@followed)
      @user.should_not be_following(@following)
    end

    it "should have a reverse_relationships method" do
      @user.should respond_to(:reverse_relationships)
    end

    it "should have the followers method" do
      @user.should respond_to(:followers)
    end

    it "should include the follower in the followers array" do
      @user.follow!(@followed)
      @followed.followers.should include(@user)
    end
  end
end

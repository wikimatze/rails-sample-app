# by usinf :user we get Factory Girl to simulate the User model
Factory.define :user do |user|
  user.name                  "Matthias Guenther"
  user.email                 "mguenther@example.com"
  user.password              "foobar"
  user.password_confirmation "foobar"
end

Factory.sequence :email do |number|
  "person-#{number}@example.com"
end

Factory.define :micropost do  |micropost|
  micropost.content "lorem ipsum"
  micropost.association :user
end


require 'spec_helper'

describe "FriendlyForwardings" do

  it "should be forward to the requested page after signin" do
    user = Factory(:user)
    visit edit_user_path(user)

    # thes test automatically follows the redirect to the signin page
    fill_in :email, :with => user.email
    fill_in :password, :with => user.password
    click_button

    # the tests follows the redirect again to users/edit
    response.should render_template('users/edit')
  end
end

require 'spec_helper'

describe "Users" do

  describe "signup" do

    describe "failure" do

      it "should not make a new user" do
        lambda do
          integration_sign_up
          response.should render_template('users/new')
          response.should have_selector("div#error_explanation")
        end.should_not change(User, :count)
      end

    end

    describe "success" do

      it "should make a new user" do
        lambda do
          user = FactoryGirl.build(:user)
          integration_sign_up(user)
          response.should have_selector("div.flash.success", :content => "Welcome")
        end.should change(User, :count).by(1)

      end

    end

  end

  describe "sign up/in" do

    describe "failure" do
      it "should not sign a user in" do
        integration_sign_in()
        click_button "Sign in"
        response.should have_selector("div.flash.error", :content => "Invalid")
      end
    end

    describe "success" do
      it "should a user sign in and out" do
        user = Factory(:user)
        integration_sign_in(user)
        controller.should be_signed_in
        click_link "Sign out"
        controller.should_not be_signed_in
      end

    end
  end

end

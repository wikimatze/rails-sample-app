require 'spec_helper'

describe PagesController do
  render_views # this ensures if the tests pass that the real page is really there so that you can test the availability of certain elements

  describe "GET 'home'" do

    describe "when not signed in" do

      before (:each) do
        get :home
      end

      it "should have correct title tag" do
        response.should have_selector("title", :content => "Matthias Guenther | home")
      end

      it "should be successful" do
        response.should be_success
      end

      it "should have title tag" do
        response.should have_selector("title", :content => "Matthias Guenther")
      end
    end

    describe "when signed in" do

      before(:each) do
        @user = test_sign_in(Factory(:user))
        other_user = Factory(:user, :email => Factory.next(:email))
        other_user.follow!(@user)
      end

      it "should have the right follower count" do
        get :home
        response.should have_selector("a", :href => following_user_path(@user),
                                           :content => "1 following")
        response.should have_selector("a", :href => followers_user_path(@user),
                                           :content => "2 followers")

      end
    end
  end

  describe "GET 'contact'" do

    before (:each) do
      get :contact
    end

    it "should have correct title tag" do
      response.should have_selector("title", :content => "Matthias Guenther | contact")
    end

    it "should be successful" do
      response.should be_success
    end

    it "should have a div with class test" do
      response.should have_selector("div", :class => "test")
    end
  end

  describe "GET 'about'" do

    before (:each) do
      get :about
    end

    it "should have correct title tag" do
      response.should have_selector("title", :content => "Matthias Guenther | about")
    end

    it "should be successful" do
      response.should be_success
    end

    it "should have a span with id test" do
      response.should have_selector("span", :id => "test")
    end
  end

  describe "GET 'help'" do
    it "should load only the base title of the page" do
      get :help
      response.should have_selector("title", :content => "Matthias Guenther")
    end
  end

end
